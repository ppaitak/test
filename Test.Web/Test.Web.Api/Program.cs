using Test.Web.Api;
var webBuilder = WebApplication.CreateBuilder(args);

var testClass1 = new TestClassChange();
testClass1.CallTestMethod();
testClass1.TestMethod();
testClass1.InvokeTestMethod();
var webApplicationBuilder = WebApplication.CreateBuilder(args);
var testClass2 = new TestClassChange();
testClass2.CallTestMethod();
testClass2.InvokeTestMethod();
// Add services to the container.

webBuilder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
webBuilder.Services.AddEndpointsApiExplorer();
webBuilder.Services.AddSwaggerGen();
webBuilder.Services.AddAuthorization();

var app = webBuilder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
